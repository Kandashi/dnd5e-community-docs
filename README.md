# dnd5e-community-docs

[Docs](https://akrigline.gitlab.io/dnd5e-community-docs/#/)

This is a community powered initiative to write down documentation for running D&D Fifth Edition.

Powered by Docsify, see those docs for more info about capabilities such as:

- [Markdown Helpers](https://docsify.js.org/#/helpers)
- [Embedded Files](https://docsify.js.org/#/embed-files)

## Contributing

Fork this project and open a Merge Request with the changes in documentation you would like considered. Please pay attention to the style guidelines (coming soon).
