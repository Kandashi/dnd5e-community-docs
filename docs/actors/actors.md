# Actors

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Common Configuration

- HP
- Armor Class
- Attributes
- Skills
- Size
- Damage Resistance/Immunity/Vulneratiblity
- Condition Immunity

## Types

## Automations

- Token Size based on Creature Size
- Armor Class Calculation & Effects

## Token Configuration

- Main Article: [Foundry KB](https://foundryvtt.com/article/tokens/)
- Resources: TempHP Bar
- Vision: Darkvision Settings
