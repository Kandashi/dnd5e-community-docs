# Home

This is a scaffolding for my ideas about an actual wiki/documentation site. The goal is to be the definitive guide to playing 5e with the vanilla system in written form so as to be googlable and searchable.

## [Home](/)

## [System Settings](system-settings.md)

## [Actors](actors/actors.md)

- [Player Characters](actors/player-characters.md)
- [NPCs](actors/npcs.md)
- [Vehicles](actors/vehicles.md)

## [Items](items/items.md)

- [Weapon](items/weapon.md)
- [Equipment](items/equipment.md)
- [Consumable](items/consumable.md)
- [Tool](items/tool.md)
- [Loot](items/loot.md)
- [Class](items/class.md)
- [Spell](items/spell.md)
- [Feature](items/feature.md)
- [Backpack](items/backpack.md)

## [Included Compendiums](included-compendiums.md)

## [Active Effects](active-effects.md)

## Guides

- [Polymorph](guides/polymorph.md)
- [Running Combat](guides/running-combat.md)
- [Troubleshooting Roll Bonuses](guides/troubleshooting-roll-bonuses.md)
