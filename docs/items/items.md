# Items

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Common Properties

- Description
- Rarity (except spells)

## Types

What does each Item type do differently from the rest?

|    Type    | Useable | Effects | Description |
|:----------:|:-------:|:-------:|-------------|
|   [Weapon](items/weapon.md)   |    ✅    |    ✅    | "Grippable": Swords, Bows, Staffs, Wands             |
|  [Equipment](items/equipment.md) |    ✅    |    ✅    | "Wearable": Armor, Shield, Clothing, Jewelry             |
| [Consumable](items/consumable.md) |    ✅    |    ✅    | "Loseable": Potions, Ammunition, Gear, Wondrous Items            |
|    [Tool](items/tool.md)    |    ✅    |         | Artisan Tools, Instruments, Some Kits            |
|    [Loot](items/loot.md)    |         |         | Misc. Gear, Trade Goods, Rewards            |
|    [Spell](items/spell.md)   |    ✅    |    ✅    | Anything that consumes a spell slot            |
|   [Feature](/wlZmKk36TB6ecGXUnPgYkA)  |    ✅    |    ✅    | Class, Racial, and Innate Passive and Active Abilities            |
|  [Backpack](items/backpack.md)  |         |         | Anything that holds other things            |

### Useable

An Item which can be configured to roll dice or do other workflows when used from an [Actor](actors/actors.md) Sheet.

The normal workflow involves triggering either a saving throw or an attack roll, possibly followed by a damage roll.

### Effects

An Item which can house [Active Effects](active-effects.md).

## Usage

- How do I roll an item on the UI?
- How do I create a macro for an item?

## Automations

These are common automations configurable for all items which can be activated (except Tool Items).

### Resource Consumption

### Attack Roll

### Prompt Saving Throw

### Damage Roll

### Ability Check(?)

_I have no idea what this does_
