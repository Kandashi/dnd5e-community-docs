# Backpack

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

![Details panel of a Backpack Item](./_images/backpack/backpack-details.png)

## Configuration

## Usage

Backpack items do not have a workflow attached, they simply print their description to chat.

## Automations

None
