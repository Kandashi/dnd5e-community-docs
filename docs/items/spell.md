# Spell

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

- Spell level
- Upcasting
- Cantrip Scaling
- Pact Magic Configuration
- Innate/At Will Config

## Usage

- Measured Template Placement
- Usual Workflow for Attack/Save + Damage

## Automations

- Spell Slot consumption
- Cantrip Scaling
- Upcasting
- Effects from Spells apply to the spell owner?
