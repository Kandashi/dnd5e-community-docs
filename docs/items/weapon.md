# Weapons

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

- Configuring an Attack
- Setting up damage formula; what inline attributes are available?
- Magical Bonuses
- Weapon Properties; which actually do things? Versatile, Finesse, Heavy?

## Usage

- Usual Workflow for Attack/Save + Damage Chat Cards

## Automations
