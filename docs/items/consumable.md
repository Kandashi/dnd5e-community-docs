# Consumables

![Up to date as of 1.4.2](https://img.shields.io/badge/dnd5e-v1.4.2-informational)

## Configuration

- Consumption + Destroy on Empty

## Usage

- Usual Workflow for Attack/Save + Damage

## Automations

- Destroy on Empty
